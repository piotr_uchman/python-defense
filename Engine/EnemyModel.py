'''
@author: Piotrek
'''

class EnemyModel(object):
    hitpoints = 5
    reward = 100
    positionX = 0
    positionY = 0
    speed = 1.0
    nextPathPoint = None
    def __init__(self, _hitpoints, _speed, _positionX, _positionY):
        self.hitpoints = _hitpoints
        self.speed = _speed
        self.positionX = _positionX
        self.positionY = _positionY
    def hit(self, damage):
        self.hitpoints -= damage
        return self.hitpoints
