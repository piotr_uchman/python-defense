'''
@author: Piotrek
'''
from Engine.WorldModel import WorldModel
from Engine.WorldModel import PathPoint
from Engine.TowerModel import TowerModel
from Engine.AmmoModel import AmmoModel
from Engine.EnemyModel import EnemyModel

class WorldFactory(object):    
    
    def createWorld(self):
        #path
        world = WorldModel()
        point = PathPoint(0, 0)
        world.pathPoints.append(point)
        point = PathPoint(150, 150)
        world.pathPoints.append(point)
        point = PathPoint(280, 420)
        world.pathPoints.append(point)
        point = PathPoint(180, 100, True)
        world.pathPoints.append(point)
        
        #towers
        ammo = AmmoModel(1, 1)
        ammo.speed = 25
        tower = TowerModel(1, 1, 50, 1, ammo, 50, 60)
        world.towers.append(tower)
        
        ammo = AmmoModel(1, 1)
        ammo.speed = 25
        tower = TowerModel(1, 1, 40, 2, ammo, 200, 200)
        world.towers.append(tower)
        
        ammo = AmmoModel(1, 1)
        ammo.speed = 25
        tower = TowerModel(1, 1, 40, 2, ammo, 280, 400)
        world.towers.append(tower)
        
        #enemies
       # enemy = EnemyModel(35, 5, 0, 0)
       # enemy.nextPathPoint = 0
       # world.enemies.append(enemy)
        
        enemy = EnemyModel(20, 15, 0, 0)
        enemy.nextPathPoint = 0
        world.enemies.append(enemy)
        
        enemy = EnemyModel(20, 10, 0, 0)
        enemy.nextPathPoint = 0
        world.enemies.append(enemy)
        
        return world