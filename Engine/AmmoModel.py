'''
@author: Piotrek
'''

class AmmoModel(object):
    ammoType = 1 #ammo type for animation
    damage = 1.0 #damage per hit
    positionX = 0 #real position X
    positionY = 0 #real position Y
    target = None #target enemy
    speed = 3.0 #ammo flight speed per second
    def __init__(self, ammoType, damage):
        self.ammoType = ammoType
        self.damage = damage
