'''
@author: Piotrek
'''
from Engine.AmmoModel import AmmoModel

class TowerModel(object):
    level = 1 #tower level
    fireRange = 10.0 
    speed = 1 #fire speed
    towerType = 1 #tower type for animation
    ammo = None #current ammo
    positionX = 0 #real position X
    positionY = 0 #real position Y
    sellCost = 0
    timeForFire = 0
    isShooting = False
    maxLevel = 2
    def __init__(self, towerType, level, fireRange, speed, ammo, positionX, positionY):
        self.towerType = towerType
        self.level = level
        self.fireRange = fireRange
        self.speed = speed
        self.ammo = ammo
        self.positionX = positionX
        self.positionY = positionY
        self.timeForFire = 1000/speed
    def fire(self, target):
        newAmmo = AmmoModel(1, self.ammo.damage);
        newAmmo.speed = self.ammo.speed;
        newAmmo.target = target
        newAmmo.positionX = self.positionX
        newAmmo.positionY = self.positionY
        return newAmmo
    def canBeUpgraded(self):
        if self.level < self.maxLevel:
            return True
        else:
            return False
    def upgrade(self):
        if self.canBeUpgraded():
            self.level += 1
            self.speed *= 1.2
            self.fireRange *= 1.2
            self.ammo.damage *= 1.5
            return True
        else:
            return False