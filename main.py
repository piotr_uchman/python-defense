import pygame
import time
import math
from Engine.WorldFactory import WorldFactory
from Engine.WorldModel import WorldModel
from Engine.TowerModel import TowerModel
from Engine.EnemyModel import EnemyModel
from Engine.AmmoModel import AmmoModel
from random import random

# Import the android module. If we can't import it, set it to None - this
# lets us test it, and check to see if we want android-specific behavior.
try:
    import android
except ImportError:
    android = None

# Event constant.
TIMEREVENT = pygame.USEREVENT

# The FPS the game runs at.
FPS = 30

# Color constants.
BLACK = (0, 0, 0, 255)
GREEN = (0, 255, 0, 255)
RED = (255, 0, 0, 255)
YELLOW = (255, 255, 0, 255)
BLUE = (0, 0, 50, 255)

def getTimeInMilliseconds():
    return int(round(time.time() * 1000))

def main():
    # world = WorldModel()
    #world.test()
    #exit()
    
    pygame.init()

    # Set the screen size.
    screen = pygame.display.set_mode((480, 500))

    # Map the back button to the escape key.
    if android:
        android.init()
        android.map_key(android.KEYCODE_BACK, pygame.K_ESCAPE)

    # Use a timer to control FPS.
    pygame.time.set_timer(TIMEREVENT, 1000 / FPS)

    # The color of the screen.
    color = BLUE
    lastTime = getTimeInMilliseconds()
    worldFactory = WorldFactory()
    worldModel = worldFactory.createWorld()
    
    builder = False
    showRange = True
    
    
    while True:
        
        ev = pygame.event.wait()

        # Android-specific:
        if android:
            if android.check_pause():
                android.wait_for_resume()
                
        
        if ev.type == pygame.MOUSEBUTTONDOWN:
            mousePos = pygame.mouse.get_pos()
            if(mousePos[0]>430 and mousePos[0]<500 and mousePos[1]>70 and mousePos[1]<120) :
                if showRange == True:
                    showRange = False
                else:
                    showRange = True
            elif(mousePos[0]>430 and mousePos[0]<500 and mousePos[1]>0 and mousePos[1]<50):
                if builder == True:
                    builder = False
                else:
                    builder = True
            elif builder == True:
                ammo = AmmoModel(1, 1)
                ammo.speed = 25
                tower = TowerModel(1, 1, 40, 2, ammo, mousePos[0], mousePos[1])
                worldModel.towers.append(tower)
                #builder = False
            else:    
                enemy = EnemyModel(20, random()*10+5, 0, 0)
                enemy.nextPathPoint = 0
                worldModel.enemies.append(enemy)
        elif ev.type == pygame.KEYDOWN and ev.key == pygame.K_ESCAPE:
            break    
        # Draw the screen based on the timer.
        elif ev.type == TIMEREVENT:
            currentTime = getTimeInMilliseconds()
            deltaTime = currentTime - lastTime
            lastTime = currentTime
            #display
            screen.fill(color)
            
            
            #engine
            if worldModel.gameOver == False:
                assert isinstance(worldModel, WorldModel)
                worldModel.fireTowers(deltaTime)
                worldModel.moveEnemies(deltaTime)
                for ammo in worldModel.ammoObjects:
                    if isinstance(ammo, AmmoModel) == False:
                        worldModel.ammoObjects.remove(ammo)
                        del ammo
                worldModel.moveAmmo(deltaTime)
                
            
            
            
            #road
            oldPoint = None;
            for pathPoint in worldModel.pathPoints:
                if oldPoint != None:
                    pygame.draw.line(screen, GREEN, (oldPoint.x, oldPoint.y), (pathPoint.x, pathPoint.y), 2)
                oldPoint = pathPoint
                

            #towers
            for tower in worldModel.towers:
                assert isinstance(tower, TowerModel)
                pygame.draw.circle(screen, GREEN, (tower.positionX, tower.positionY),5 ,0);
                #range
                if showRange:
                    pygame.draw.circle(screen, GREEN, (tower.positionX, tower.positionY), tower.fireRange ,1);
                
                
            #enemies
            for enemy in worldModel.enemies:
                assert isinstance(enemy, EnemyModel)
                pygame.draw.circle(screen, RED, (math.trunc(enemy.positionX), math.trunc(enemy.positionY)),3, 0)
                
            #missles
            for missle in worldModel.ammoObjects:
                assert isinstance(missle, AmmoModel)
                pygame.draw.circle(screen, YELLOW, (math.trunc(missle.positionX), math.trunc(missle.positionY)), 2, 0)

            #builderButton
            if builder == True:
                pygame.draw.rect(screen,GREEN,pygame.Rect(430,0,50,50))
            else:
                pygame.draw.rect(screen,RED,pygame.Rect(430,0,50,50))
                
            if showRange == True:
                pygame.draw.rect(screen,GREEN,pygame.Rect(430,70,50,50))
            else:
                pygame.draw.rect(screen,RED,pygame.Rect(430,70,50,50))
        
            #display
            
            pygame.display.flip()



# This isn't run on Android.
if __name__ == "__main__":
    main()
